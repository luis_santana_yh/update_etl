![yandeh.png](https://bitbucket.org/repo/GkGbr7/images/1173725914-yandeh.png)
# ETL: Update Daruma #
 
## Esta aplicação consegue:
* *Parte 1:* Ler arquivo CSV.
* *Parte 2:* Drop tabela Cupom.
* *Parte 3:* Carrega dados CSV para a tabela Cupom.
* *Parte 4:* Realiza Update na tabela VENDA_CUPOM_ITEM.
##